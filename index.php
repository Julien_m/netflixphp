<?php
session_start();

require('src/log.php');

if (!empty($_POST['email']) && !empty($_POST['password'])) {

	require('src/connect.php');

	// VAR
	$email = htmlspecialchars($_POST['email']);
	$password = htmlspecialchars($_POST['password']);

	// EMAIL VALID
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		header('location: index.php?error=1&message=Votre adresse email est invalide');
		exit();
	}

	// PASSWORD ENCRYPTION
	$password = "aq1" . sha1($password . "123") . "25";

	// CORRECT EMAIL
	$req = $db->prepare("SELECT count(*) as numberEmail FROM user WHERE email = ?");
	$req->execute(array($email));

	while ($email_verification = $req->fetch()) {
		if ($email_verification['numberEmail'] != 1) {
			header('location: index.php?error=1&message=Combinaison incorrect !');
			exit();
		}
	}

	// CONNEXION
	$req = $db->prepare("SELECT * FROM user WHERE email = ?");
	$req->execute(array($email));

	while ($user = $req->fetch()) {

		if ($password == $user['password'] && $user['blocked'] == 0) {

			$_SESSION['connect'] = 1;
			$_SESSION['email'] = $user['email'];

			if (isset($_POST['auto'])) {
				setcookie('auth', $user['secret'], time() + 364 * 24 * 3600, '/', null, false, true);
			}

			header('location: index.php?success=1');
			exit();
		} else {
			header('location: index.php?error=1&message=Combinaison incorrect !');
		}
	}
}


if (!empty($_POST['movie'])) {

	// We replace the spaces by a '+' to not bug the API
	$movieSearch = preg_replace("/[\s-]+/", "+", $_POST['movie']);


	$apiKey = "";
	$json = file_get_contents("https://api.themoviedb.org/3/search/movie?api_key={$apiKey}&language=fr-FR&query=" . $movieSearch);

	$parsee = json_decode($json, true);
	// var_dump($parsee['results'][0]['title']);


	if (isset($parsee['results'][0])) {
		// var_dump($parsee['results'][0]);

		$movies = array();
		for ($i = 0; $i < 12; $i++) {
			if ($i < count($parsee['results'])) {
				array_push($movies, $parsee['results'][$i]);
			}
		}
		// var_dump ($movies);
	} else {
		header("location: index.php?error=1&message=Votre recherche n'a donné aucun resultat.");
		exit();
	}
}
?>

<?php include('src/head.php'); ?>

<body>

	<?php include('src/header.php'); ?>


	<section>
		<?php if (isset($_SESSION['connect'])) {

			?>
			<div id="body-search">

				<div class="center">

					<p>Qu'allez-vous regarder aujourd'hui ?</p>
					<form method="post" action="index.php">
						<input type="text" name="movie" placeholder="Nom du film" required />
						<button type="submit">Rechercher</button>
					</form>
					<?php
					if (isset($movies)) {
						foreach ($movies as $movie) {
							echo "
							<img src='https://image.tmdb.org/t/p/w300{$movie['poster_path']}'  class='image' />";
						}
					} else if (isset($_GET['error'])) {
						echo "<small>".htmlspecialchars($_GET['message'])."</small>";
					}

					?>
				</div>
			</div>
		<?php } else { ?>
			<div id="login-body">
				<h1>S'identifier</h1>

				<?php

				if (isset($_GET['error'])) {

					if (isset($_GET['message'])) {
						echo '<div class="alert error">' . htmlspecialchars($_GET['message']) . '</div>';
					}
				}
				?>
				<form method="post" action="index.php">
					<input type="email" name="email" placeholder="Votre adresse email" required />
					<input type="password" name="password" placeholder="Mot de passe" required />
					<button type="submit">S'identifier</button>
					<label id="option"><input type="checkbox" name="auto" checked />Se souvenir de moi</label>
				</form>


				<p class="grey">Première visite sur Netflix ? <a href="inscription.php">Inscrivez-vous</a>.</p>
			<?php } ?>
		</div>
	</section>


	<?php include('src/footer.php'); ?>

</body>

</html>
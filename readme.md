# Projet Netflixx par MAILLE Julien

Languages utilisés
* HTML
* CSS
* PHP 7
* SQL

Features du projet
* Design à la main basé sur Netflix
* Responsive design via les Media Queries
* Système d'inscription sécurisé, double verification que le champ email soit bien un email, verification que l'email est libre
* Système de connexion
* Utilisation des sessions
* Si la checkbox "Se souvenir de moi" est cochée, utilisation d'un cookie pour garder la connexion
* Système de recherche de film et affichage des affiches via l'API EXTERNE "ThemovieDB"
* Page profil qui permet de modifier son adresse mail et/ou son mot de passe
* Gestion des erreurs et affichage d'un message explicite
* Système de déconnexion qui vient fermer la session et supprimer le cookie

*********************

# Netflixx project by MAILLE Julien

Languages ​​used
* HTML
* CSS
* PHP 7
* SQL

Project Features
* Handmade design based on Netflix
* Responsive design via Media Queries
* Secure registration system, double verification that the email field is an email, verification that the email is free
* Connection system
* Using sessions
* If checkbox "Remember me" is checked, use a cookie to keep the connection
* Film search system and display of posters via the EXTERNAL API "ThemovieDB"
* Profile page that allows you to change your email address and / or password
* Error handling and display of an explicit message
* Disconnection system that comes to close the session and delete the cookie
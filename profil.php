<?php
session_start();

require('src/log.php');

if (!isset($_SESSION['connect'])) {
    header('location: index.php');
    exit();
} else {
    require('src/connect.php');
    // VAR
    $secret = htmlspecialchars($_COOKIE['auth']);

    // VERIFICATION
    require('src/connect.php');

    $req =  $db->prepare("SELECT count(*) as numberAccount FROM user WHERE secret = ?");
    $req->execute(array($secret));

    while ($user = $req->fetch()) {
        if ($user['numberAccount'] == 1) {

            $reqUser = $db->prepare("SELECT * FROM user WHERE secret = ? ");
            $reqUser->execute(array($secret));

            while ($userAccount = $reqUser->fetch()) {

                $_SESSION['connect'] = 1;
                $_SESSION['email'] = $userAccount['email'];
            }
        }
    }


    if (!empty($_POST['email']) && !empty($_POST['password'])) {
        $email = htmlspecialchars($_POST['email']);
        $password = htmlspecialchars($_POST['password']);

        $password = "aq1" . sha1($password . "123") . "25";


        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

            header('location: profil.php?error=1&message=Adresse mail est invalide.');
            exit();
        }

        if (!empty($_POST['password_new']) && !empty($_POST['password_new_two'])) {
            $password_new = htmlspecialchars($_POST['password_new']);
            $password_new_two = htmlspecialchars($_POST['password_new_two']);
            $password_new = "aq1" . sha1($password_new . "123") . "25";
            $password_new_two = "aq1" . sha1($password_new_two . "123") . "25";
            if ($_POST['password_new'] == $_POST['password_new_two']) {
                if ($email = $_SESSION['email']) {
                    $req =  $db->prepare("UPDATE user SET password = ? WHERE secret = ? AND password = ?");
                    $req->execute(array($password_new, $secret, $password));
                    $count = $req->rowCount();
                    if ($count > 0) {
                        header('location: profil.php?success=1&message=Votre mot de passe a été modifié avec succès.');
                        exit();
                    } else {
                        header('location: profil.php?error=1&message=Votre mot de passe est incorrect.');
                        exit();
                    }
                } else {
                    $req =  $db->prepare("UPDATE user SET password = ? , email = ? WHERE secret = ? AND password = ?");
                    $req->execute(array($password_new, $email, $secret, $password));
                    $count = $req->rowCount();
                    if ($count > 0) {
                        header('location: profil.php?success=1&message=Votre mot de passe a été modifié avec succès.');
                        exit();
                    } else {
                        header('location: profil.php?error=1&message=Votre mot de passe est incorrect.');
                        exit();
                    }
                }
            } else {
                header('location: profil.php?error=1&message=Vos mots de passes ne sont identiques.');
                exit();
            }
        } else {
            if ($email != $_SESSION['email']) {
                $req =  $db->prepare("UPDATE user SET email = ? WHERE secret = ? AND password = ?");
                $req->execute(array($email, $secret, $password));
                $count = $req->rowCount();
                if ($count > 0) {
                    header('location: profil.php?success=1&message=Votre email a été modifié avec succès.');
                    exit();
                } else {
                    header('location: profil.php?error=1&message=Votre mot de passe est incorrect.');
                    exit();
                }
            } else {
                header("location: profil.php?success=1&message=Aucune modification n'a été demandée.");
                exit();
            }
        }
    }
}

?>

<?php include('src/head.php'); ?>

<body>

    <?php include('src/header.php'); ?>

    <div id="profil-body">
        <h1>Modifier votre profil</h1>
        <?php if (isset($_GET['error'])) {

            if (isset($_GET['message'])) {

                echo '<div class="alert error">' . htmlspecialchars($_GET['message']) . '</div>';
            }
        } else if (isset($_GET['success'])) {

            echo '<div class="alert success">' . htmlspecialchars($_GET['message']) . '</div>';;
        }
        ?>
        <form method="post" action="profil.php">
            <input type="email" name="email" placeholder="Votre adresse email" value="<?php echo $_SESSION['email'] ?>" required />
            <input type="password" name="password" placeholder="Mot de passe actuel (nécessaire pour modifier les informations du profil)" required />
            <small>Ne remplir que si vous souhaitez changer votre mot de passe.</small>
            <input type="password" name="password_new" placeholder="Nouveau mot de passe" />
            <input type="password" name="password_new_two" placeholder="Retapez votre nouveau mot de passe" />
            <button type="submit">Modifier le profil</button>
            <div class="right"><small><a href="index.php">Retourner à l'accueil</a></small></div>
        </form>
    </div>

    <?php include('src/footer.php'); ?>

</body>

</html>